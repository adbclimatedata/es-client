import csv
from enum import Enum
import json
import jsonschema as jsonschema
import configparser
import utility
import datetime
import time
from slugify import slugify

__author__ = 's2789047'

# Reference: http://wiki.openstreetmap.org/wiki/Key:place
ordered_place_names = ["continent", "country", "state", "province",
                       "district", "county", "region", "municipality",
                       "city", "borough", "suburb", "quarter",
                       "neighbourhood", "city_block", "plot",
                       "town", "village", "hamlet", "isolated_dwelling",
                       "farm", "allotments", "archipelago",
                       "island", "islet", "locality"]

class CSVImporter:

    def __init__(self, filenames, schema_filename, cfg_filename, es_client_handler):
        self.filenames = filenames
        self.schema_filename = schema_filename
        self.cfg_filename = cfg_filename
        self.es_client_handler = es_client_handler

    def import_csv_contents_to_elastic_search(self):
        with open(self.schema_filename) as json_metadata_file:
            schema = json.load(json_metadata_file)

        cfg = configparser.ConfigParser()
        cfg.read([self.cfg_filename])

        for filename in self.filenames:
            json_parser = CSVToJSONParser(filename, cfg, schema)
            json_array = json_parser.parse_csv_into_json_array()

            print ("Number of metadata contents: " + str(len(json_array)))

            for json_data in json_array:
                try:
                    self.__post_processing_json_data__(json_data)
                    jsonschema.validate(json_data, schema)
                    json_id = json_data['id']
                    print("Trying to INDEX: " + str(json_id))
                    if not self.es_client_handler.exists(json_id):
                        #TODO: uncomment/comment if indexing with ElasticSearch is needed/not needed.
                        print("SUCCESS: Indexed: " + str(json_id))
                        self.es_client_handler.index_item(json_data, id = json_id)
                    else:
                        print("FAIL: INDEX exists already: " + str(json_id))
                except jsonschema.ValidationError as e:
                    print (e.message)
                    print ("For the instance: " + str(json_data))
                except jsonschema.SchemaError as e:
                    print (e.message)

    def __post_processing_json_data__(self, json_data):
        self.__process_status_field__(json_data)
        self.__process_geographic_coverage_field(json_data)
        self.__process_geographic_path_field(json_data)

    def __process_status_field__(self, json_data):
        json_data["status"] = "Published"

        ts = time.time()
        timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
        json_data['timestamp'] = timestamp

        
        
        json_data['slug'] = slugify(str(json_data['title']))


  
    

    def __process_geographic_coverage_field(self, json_data):
        if "geographic-coverage" in json_data:
            if json_data["geographic-coverage"] != "N/A":
                coverage_array = self.__generate_geographic_coverage__(json_data["geographic-coverage"])
                location_pins = self.__generate_geographic_pins__(json_data["geographic-coverage"])
                if coverage_array:
                    json_data["geographic-coverage"] = coverage_array
                else:
                    del json_data["geographic-coverage"]

                if location_pins:
                        json_data["geographic-pin"] = location_pins
                        # print 'PINS'
                        # print location_pins


                # else:
                    # del json_data["geographic-pin"]
            else:
              del json_data["geographic-coverage"]

    def __generate_geographic_coverage__(self, geographic_coverage):
        converted_geographic_coverage = []
        for item in geographic_coverage:
            if "place_id" in item:
                coverage = {}
                coverage["display-name"] = item["display_name"]
                # In boundingbox: order - south latitude (0), north latitude (1), west longitude (2), east longitude (3)
                coverage["extents"] = {}
                coverage["extents"]["northern-extent"] = item["boundingbox"][1]
                coverage["extents"]["eastern-extent"] = item["boundingbox"][3]
                coverage["extents"]["southern-extent"] = item["boundingbox"][0]
                coverage["extents"]["western-extent"] = item["boundingbox"][2]
                coverage["latitude"] = item["lat"]
                coverage["longitude"] = item["lon"]
                coverage["address"] = item["address"]
                converted_geographic_coverage.append(coverage)

        return converted_geographic_coverage

    def __generate_geographic_pins__(self, geographic_coverage):
        pins = []
        for item in geographic_coverage:
            if "place_id" in item:
                location = {}
                
                # In boundingbox: order - south latitude (0), north latitude (1), west longitude (2), east longitude (3)
                location["location"] = {}
                location["location"]["lat"] = item["lat"]
                location["location"]["lon"] = item["lon"]
                
                pins.append(location)

        return pins

    def __process_geographic_path_field(self, json_data):
        if "geographic-coverage" in json_data:
            geographic_paths = []
            for item in json_data["geographic-coverage"]:
                geographic_path = self.__convert_address_to_geographic_path__(item["address"])
                if geographic_path:
                    geographic_paths.append(geographic_path)

            if geographic_paths:
                json_data["geographic-path"] = geographic_paths

    def __convert_address_to_geographic_path__(self, address):
        geographic_path = ''
        for index, item in enumerate(ordered_place_names, start=0):   # default is zero
            if item in address:
                geographic_path += "/" + address[item]

        #remove first slash
        geographic_path = geographic_path[1:]
        return geographic_path




class FieldType(Enum):
    single_text = 1
    array_text = 2
    single_number = 3
    single_boolean = 4
    object_properties = 5
    array_object = 6


#parses a CSV file, and provides methods to return an array of json as per the configuration and schema
class CSVToJSONParser:

    def __init__(self, filename, cfg, schema):
        self.filename = filename
        self.cfg = cfg
        self.schema = schema

    def parse_csv_into_json_array(self):
        json_array = []
        with open(self.filename) as csvfile:
            csvreader = csv.DictReader(csvfile)
            for row in csvreader:
                #FixMe: It is a manual checking to stop grabbing the row content. Find a better way.
                if row['Title of content'] is '':
                    break
                json_data = self.build_json_metadata(row)
                json_array.append(json_data)

        return json_array

    def build_json_metadata(self, metadata_dict):
        metadata_json = {}

        cfg_group = "fields"

        property_dict = self.schema['properties']
        for schema_field_name in property_dict.keys():
            if str(property_dict[schema_field_name]['type']) == 'string':
                self.set_json_fieldvalue_from_dict(schema_field_name, cfg_group, metadata_dict, metadata_json, FieldType.single_text)
            elif str(property_dict[schema_field_name]['type']) == 'array':
                if str(property_dict[schema_field_name]['items']['type']) == 'string':
                    self.set_json_fieldvalue_from_dict(schema_field_name, cfg_group, metadata_dict, metadata_json, FieldType.array_text)
                #TODO: Need to add support for this option. For example, 'authors' field is of this category
                elif str(property_dict[schema_field_name]['items']['type']) == 'object':
                    self.set_json_fieldvalue_from_dict(schema_field_name, cfg_group, metadata_dict, metadata_json, FieldType.array_object)
            elif str(property_dict[schema_field_name]['type']) == 'object':
                #print (schema_field_name)
                self.set_json_fieldvalue_from_dict(schema_field_name, cfg_group, metadata_dict, metadata_json, FieldType.object_properties)
            elif str(property_dict[schema_field_name]['type']) == 'number':
                self.set_json_fieldvalue_from_dict(schema_field_name, cfg_group, metadata_dict, metadata_json, FieldType.single_number)
            elif str(property_dict[schema_field_name]['type']) == 'boolean':
                self.set_json_fieldvalue_from_dict(schema_field_name, cfg_group, metadata_dict, metadata_json, FieldType.single_boolean)



        '''
        #TODO: Fix me: support for geographic-coverage is needed. It is a special type 'oneOf'
        #The following fields do not exist in the CSV file. They need to be handled if they exist later.
        #metadata_json['geographic-coverage'] = None

        #metadata_json['content-owners'] = None
        #metadata_json['content-storage-link'] = None
        #metadata_json['contact-person'] = None # Need to find a sample
        #metadata_json['data-sharing-allowed'] = True
        #metadata_json['authors'] = None
        #metadata_json['associated-files'] = None
        '''

        return metadata_json


    def set_json_fieldvalue_from_dict(self, fieldname, cfg_group, metadata_dict, metadata_json, fieldtype):

        if fieldname in self.cfg.options(cfg_group):
            mapped_fieldname = self.cfg.get(cfg_group, fieldname)
            if mapped_fieldname in metadata_dict and metadata_dict[mapped_fieldname] is not '':
                if fieldtype is FieldType.single_text:
                    metadata_json[fieldname] = metadata_dict[mapped_fieldname]
                elif fieldtype is FieldType.array_text:
                    metadata_json[fieldname] = utility.delimited_string_to_string_array(metadata_dict[mapped_fieldname], ',')
                elif fieldtype is FieldType.array_object:

                    try:
                        metadata_json[fieldname] = json.loads(metadata_dict[mapped_fieldname])
                    except ValueError:
                        print("The format of the input is incorrect for '" + mapped_fieldname + "' field")
                        print("The value given: " + metadata_dict[mapped_fieldname])
                        print("Expected format: An array of JSON objects")
                elif fieldtype is FieldType.single_number:
                    metadata_json[fieldname] = int(metadata_dict[mapped_fieldname].strip())
                elif fieldtype is FieldType.single_boolean:
                    metadata_json[fieldname] = bool(metadata_dict[mapped_fieldname].strip())
            elif fieldtype is FieldType.object_properties:
                    mapped_property_fieldname_pairs = utility.delimited_string_to_string_array(mapped_fieldname, ',')
                    composite_field_json = {}
                    for mapped_property_fieldname_pair in mapped_property_fieldname_pairs:
                        mapped_property_fieldnames = utility.delimited_string_to_string_array(mapped_property_fieldname_pair, ':')
                        property_original_fieldname = mapped_property_fieldnames[0]
                        mapped_property_fieldname = mapped_property_fieldnames[1]

                        if mapped_property_fieldname in metadata_dict and metadata_dict[mapped_property_fieldname] is not '':
                            #Note: here, we handle only 'string', 'number', 'boolean'
                            if str(self.schema['properties'][fieldname]['properties'][property_original_fieldname]['type']) == 'string':
                                composite_field_json[property_original_fieldname] = metadata_dict[mapped_property_fieldname]
                            elif str(self.schema['properties'][fieldname]['properties'][property_original_fieldname]['type']) == 'number':
                                composite_field_json[property_original_fieldname] = int(metadata_dict[mapped_property_fieldname])
                            elif str(self.schema['properties'][fieldname]['properties'][property_original_fieldname]['type']) == 'boolean':
                                composite_field_json[property_original_fieldname] = bool(metadata_dict[mapped_property_fieldname])

                    if bool(composite_field_json):
                        metadata_json[fieldname] = composite_field_json