import json
import uuid

from elasticsearch import Elasticsearch
import jsonschema
import csvhandler

__author__ = 's2789047'


class ElasticSearchClientHandler:
    index = "rccdf-repository" # TODO: make them None
    doc_type = "_all"
    hosts = [{'host': 'localhost', 'port': 9200}]
    es_client =  None

    def __init__(self, hosts = None, index = None, doc_type = None):
        if hosts != None:
            self.hosts = hosts

        if index != None:
            self.index = self.convert_to_valid_str(index)

        if doc_type != None:
            self.doc_type = self.convert_to_valid_str(doc_type)

        self.es_client = Elasticsearch(self.hosts)

    def convert_to_valid_str(self, a_string):
        return str.replace(str(a_string), ' ', '_').lower()


    def index_item(self, metadata, index = None, doc_type = None, id = None):
        index_name = self.convert_to_valid_str(index) if index != None else self.index
        doc_type_name = self.convert_to_valid_str(doc_type) if doc_type != None else self.doc_type

        res = self.es_client.index(index = index_name, doc_type = doc_type_name, id = id, body = metadata)
        return res

    def delete_item(self, id, index = None, doc_type = None):
        index_name = self.convert_to_valid_str(index) if index != None else self.index
        doc_type_name = self.convert_to_valid_str(doc_type) if doc_type != None else self.doc_type

        res = self.es_client.delete(index = index_name, doc_type = doc_type_name, id = id)
        return res

    def get_all_items(self, index=None, doc_type=None):
        res = self.es_client.search(index = index, doc_type = doc_type, body = {"query": {"match_all": {}}})
        return res

    def get_item_by_id(self, id, index = None, doc_type = None):
        index_name = self.convert_to_valid_str(index) if index != None else self.index
        doc_type_name = self.convert_to_valid_str(doc_type) if doc_type != None else self.doc_type
        res = self.es_client.get(index = index_name, doc_type = doc_type_name, id = id)
        return res

    def refresh_indices(self, index = None):
        self.es_client.indices.refresh(index = index)

    # body can take two types of update: 'script' and 'doc'
    def update_item(self, index, doc_type, id, body = None):
        res = self.es_client.update(index, doc_type, id, body = body)
        return res

    def exists(self, id, params=None):
        return self.es_client.exists(self.index, self.doc_type, id, params=params)

def index_metadata_from_samples():
    schema_filename = "schema/metadata-schema.json"
    with open(schema_filename) as json_metadata_file:
            schema = json.load(json_metadata_file)

    #hosts = [{'host': '203.101.228.255', 'port': 9200}]
    #temporary change of ip and port
    hosts = [{'host': '127.0.0.1', 'port': 9200}]
    index_name = 'repo_test' #change the number for each test
    doc_type = 'item'
    for x in range(1, 8):
        filename = "samples/metadata_" + str(x) + ".json"
        with open(filename) as json_metadata_file:
            metadata = json.load(json_metadata_file)
            json_id = metadata['id']
            es_client_wrapper = ElasticSearchClientHandler(hosts = hosts,
                                        index = index_name,
                                        doc_type = doc_type)
            jsonschema.validate(metadata, schema)
            es_client_wrapper.index_item(metadata, id = json_id)



def index_metadata_from_csv():

    filenames = []
    for x in range(1, 8):
        #print ("We're on time %d" % (x))
        filename = "csv/metadata_" + str(x) + ".csv"
        filenames.append(filename)

    schema_filename = "schema/metadata-schema.json"
    cfg_filename = "csv/fieldmap.cfg"

    index_name = 'adb_repo'
    doc_type = 'item'


    #hosts = [{'host': '203.101.228.255', 'port': 9200}]
    #hosts = [{'host': '127.0.0.1', 'port': 9200}]
    hosts = [{'host': '192.168.99.100', 'port': 9200}] # Docker one
    es_client_handler = ElasticSearchClientHandler(hosts = hosts,
                                        index = index_name,
                                        doc_type = doc_type)

    csv_importer = csvhandler.CSVImporter(filenames, schema_filename, cfg_filename, es_client_handler)
    csv_importer.import_csv_contents_to_elastic_search()

def generate_unique_ids(count):
    for i in range(count):
        print(str(uuid.uuid1()))

if __name__ == "__main__":

    #Generate Unique IDs
    #generate_unique_ids(100)

    #Import from sample JSON files
    # index_metadata_from_samples()

    #Import from a CSV file
    index_metadata_from_csv()

