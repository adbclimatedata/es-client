__author__ = 's2789047'


#It parses the comma separated string, and creates string array
def delimited_string_to_string_array(delimited_string, separator):
    all_strings = delimited_string.strip().split(separator)
    string_array = []
    for one_string in all_strings:
        string_array.append(one_string.strip())

    return string_array